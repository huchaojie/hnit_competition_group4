package com.hynu.epidemic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//在SpringBootApplication上使用@ServletComponentScan注解后，Servlet、Filter、Listener可以直接通过@WebServlet、@WebFilter、@WebListener注解自动注册，无需其他代码。
@ServletComponentScan
@SpringBootApplication
@MapperScan("com.hynu.epidemic.mapper") //扫描Mybatis中所有的映射接口
public class EpidemicHelperApplication {
    public static void main(String[] args) {
        SpringApplication.run(EpidemicHelperApplication.class, args);
    }
}

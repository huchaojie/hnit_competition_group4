package com.hynu.epidemic.entity;
/**
 * 请假
 */

import lombok.Data;

@Data
public class Holiday {
    private  Integer hid; //编号
    private  Integer sid;//学号
    private  Integer tid;//教师编号
    private  String hreason;//请假理由
    private  String htype;//请假类型
    private  String htime;//请假申请时间
    private  String hbegintime;//请假开始时间
    private  String hendtime;//请假结束时间
    private  Integer hday;//请假天数

}

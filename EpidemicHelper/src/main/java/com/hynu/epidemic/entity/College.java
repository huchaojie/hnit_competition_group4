package com.hynu.epidemic.entity;
/**
 * 学院
 */

import lombok.Data;

@Data
public class College {
    private  Integer cid;//学院编号
    private  String cname;//学院名称

}

package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 学生
 */
@Data
public class Student {
    private Integer sid;//学号
    private String sname;//姓名
    private String spwd;//密码
    private String sbuild;//楼栋'13A'
    private String sdormno;//宿舍号
    private Integer mid;//专业编号
    private Integer cid;//班级编号
    private String ssex;//性别
    private String sphone;//电话号码
}

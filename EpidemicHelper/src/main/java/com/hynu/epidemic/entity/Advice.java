package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 通知
 */
@Data
public class Advice {
    private Integer aid;//编号
    private String atime;//发布时间
    private String acontent;//内容
}

package com.hynu.epidemic.entity.vo;

import com.hynu.epidemic.enums.ResultEnum;
import lombok.Data;
import lombok.ToString;

/**
 * @program: springboot-snacknet
 * @description: 结果视图类
 * @author: Lydia
 * @create: 2021-08-23 21:28
 */
@ToString
@Data
public class ResultVO {
    private Integer code;
    private String msg;
    private Object data;

    public ResultVO() {
    }

    public ResultVO(ResultEnum enums) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }

    public ResultVO(ResultEnum enums, Object data) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
        this.data = data;
    }

    public ResultVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultVO(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}

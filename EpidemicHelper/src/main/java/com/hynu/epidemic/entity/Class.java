package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 班级
 */
@Data
public class Class {
    private Integer cid;//班级
    private Integer cgrade;//年纪
    private String cname;//班级名称
    private  Integer mid;//专业编号
    private  Integer tid;//辅导员编号
    private String tphone;//辅导员电话
}

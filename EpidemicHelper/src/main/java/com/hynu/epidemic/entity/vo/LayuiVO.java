package com.hynu.epidemic.entity.vo;

import com.hynu.epidemic.enums.ResultEnum;
import lombok.Data;

/**
 * 值对象
 */
@Data
public class LayuiVO {
    private Integer code = 0; // 状态码
    private String msg = ""; // 提示信息
    private Object data; // 数据
    private Integer count = 0; // 总记录数

    public LayuiVO() {
        super();
    }

    public LayuiVO(ResultEnum enums) {
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }

    public LayuiVO(Object data) {
        this.data = data;
    }

    public LayuiVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public LayuiVO(Integer count, Object data) {
        this.count = count;
        this.data = data;
    }

    public LayuiVO(Integer code, Integer count, Object data) {
        this.code = code;
        this.count = count;
        this.data = data;
    }
}

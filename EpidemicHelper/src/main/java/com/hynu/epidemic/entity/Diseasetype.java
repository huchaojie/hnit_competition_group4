package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 病例
 */
@Data
public class Diseasetype {
    private Integer did;//编号
    private String dname;//名称
    private String ddepict;//描述
}

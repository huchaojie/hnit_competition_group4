package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 药品交换
 */
@Data
public class Exchange {
    private Integer eid;//编号
    private String mtype;//药品类型
    private String ephone;//电话号码
    private String eaddr;//地址
}

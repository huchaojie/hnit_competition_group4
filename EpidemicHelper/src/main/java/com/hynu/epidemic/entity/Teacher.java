package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 教师
 */
@Data
public class Teacher {
    private Integer tid;//教师编号
    private String tname;//姓名
    private String tpwd;//密码
    private Integer cid;//学院编号
    private Integer tphone;//教师电话
    private String ttype;//教师类型
}

package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 专业
 */

@Data
public class Majio {
    private  Integer mid;//专业编号
    private String mname;//专业名称
    private String cid;//学院编号

}

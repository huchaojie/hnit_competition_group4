package com.hynu.epidemic.entity;

import lombok.Data;

/**
 * 药品领取表
 */
@Data
public class Medicreceive {
    private Integer mid;//编号
    private Integer sid;//学号/教师编号
    private String mtype;//药品类型
    private Integer mcount;//数量
    private String mtime;//申请时间
    private String mrecetime;//领取时间
    private Integer mstate;//状态 1通过，2已领取，3暂缺
    private String mdepict;//描述
}

package com.hynu.epidemic.entity;

import com.mysql.jdbc.Blob;
import lombok.Data;

/**
 * 结果
 */
@Data
public class Result {
    private Integer rid;//编号
    private  Integer sid;//学号/教师编号
    private Integer did;//疫情类型编号
    private String rbarcode;//条形码
    private String rresult;//结果（阳/正常）
    private Blob rprove;//证明材料（图片）
    private String datetime;//上传时间
}

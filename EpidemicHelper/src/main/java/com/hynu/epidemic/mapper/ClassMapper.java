package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Class;

import java.util.List;

public interface ClassMapper {
    public List<Class> find();
}

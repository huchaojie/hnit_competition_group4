package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Medicreceive;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MedicreceiveMapper {
    public List<Medicreceive> find();
    public int m_insert(@Param("stuId") int sid,@Param("v1") String v1,@Param("v2") int v2,@Param("mtime") String mtime);
}

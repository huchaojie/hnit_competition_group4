package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Teacher;

import java.util.List;

public interface TeacherMapper {
    public List<Teacher> find();
}

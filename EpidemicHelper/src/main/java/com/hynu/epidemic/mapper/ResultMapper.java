package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Result;
import com.mysql.jdbc.Blob;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ResultMapper {
    public List<Result> find();
    public int insert(@Param("stuId") int sid,@Param("v1") Blob v1,@Param("format.format(d)") String datetime);
}

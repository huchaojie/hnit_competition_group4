package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface StudentMapper {
    public List<Student> find();
    public List<Student> login(@Param("v1") String v1,@Param("v2") String v2);
}

package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Advice;

import java.util.List;

public interface AdviceMapper {
    public List<Advice> find();
}

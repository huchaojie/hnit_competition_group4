package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.College;

import java.util.List;

public interface CollegeMapper {
    public List<College> find();
}

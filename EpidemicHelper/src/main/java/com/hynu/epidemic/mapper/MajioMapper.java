package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Majio;

import java.util.List;

public interface MajioMapper {
    public List<Majio> find();
}

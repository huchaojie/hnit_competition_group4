package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Holiday;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HolidayMapper {
    /**
     * 根据编号查询
     */
    public List<Holiday> find();

    /**
     * 添加操作
     */
    public int insert(@Param("stuId") int sid,@Param("htime") String htime,@Param("v1") String v1,@Param("v2") String v2,@Param("v3") String v3);

}

package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Exchange;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExchangeMapper {
    public List<Exchange> find();

    public int e_insert(@Param("v1") String v1,@Param("v2") String v2, @Param("v3") String v3);

    public List<Exchange> e_select(@Param("v1") String v1,@Param("v2") String v2);
}

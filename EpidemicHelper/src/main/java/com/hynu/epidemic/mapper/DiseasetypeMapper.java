package com.hynu.epidemic.mapper;

import com.hynu.epidemic.entity.Diseasetype;

import java.util.List;

public interface DiseasetypeMapper {
    public List<Diseasetype> find();
}

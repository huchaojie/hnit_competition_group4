package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Exchange;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IExchangeService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/exchange")
public class ExchangeController {
    @Autowired
    private IExchangeService exchangeService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Exchange> list = exchangeService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }

    @PostMapping("exch")
    public ResultVO e_insert(@RequestParam("mtype") String v1, @RequestParam("ephone") String v2,@RequestParam("eaddr") String v3){
        ResultVO r =new ResultVO();
        int a=exchangeService.e_insert(v1,v2,v3);
        if (a==0){
            r.setCode(500);
            r.setMsg("药品互换申请提交失败");
        }else {
            r.setCode(200);
            r.setMsg("药品互换申请提交成功");
        }
        return r;
    }

    @PostMapping("ins")
    public List<Exchange> e_select(@RequestParam("mtype") String v1, @RequestParam("ephone") String v2){
        ResultVO r =new ResultVO();
        List<Exchange> a=exchangeService.e_select(v1,v2);
        if (a==null||a.size()<=0) {
            r.setCode(500);
            r.setMsg("暂无信息");
        }else {
            r.setCode(200);
        }
        return a;
    }

}

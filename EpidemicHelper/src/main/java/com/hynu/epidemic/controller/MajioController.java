package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Majio;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IMajioService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/majio")
public class MajioController {
    @Autowired
    private IMajioService majioService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Majio> list = majioService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }
}

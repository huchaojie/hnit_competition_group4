package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Student;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IStudentService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 学生相关的控制器
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private IStudentService studentService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Student> list = studentService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }

    /**
     * 登录功能
     * @param v1
     * @param v2
     * @return
     */
    @PostMapping("/login")
    /**
     * v1是账号 v2是密码
     */
    public ResultVO denglu(@RequestParam("sid") String v1, @RequestParam("spwd") String v2, HttpSession session){
        List<Student> a=studentService.login(v1,v2);
        ResultVO r =new ResultVO();
        if (a==null||a.size()<=0) {
            r.setCode(500);
            r.setMsg("用户名或密码输入错误，请重新输入");
        }else {
            session.setAttribute("StudentInfo",a.get(0));
            r.setCode(200);
        }
        return r;
    }
}

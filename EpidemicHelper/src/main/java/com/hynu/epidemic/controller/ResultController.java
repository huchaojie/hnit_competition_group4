package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Result;
import com.hynu.epidemic.entity.Student;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IResultService;
import com.hynu.epidemic.entity.vo.ResultVO;
import com.mysql.jdbc.Blob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/result")
public class ResultController {
    @Autowired
    private IResultService resultService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Result> list = resultService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }

    /**
     * 结果上传添加操作
     * @param v1
     * @param session
     * @return
     */
    @PostMapping("commitimg")
    /**
     * 结果上传，v1是证明材料（图片）
     */
    public ResultVO insert(@RequestParam("rprove") Blob v1, HttpSession session){
        ResultVO r =new ResultVO();

        if(null == session){
            r.setCode(500);
            r.setMsg("数据异常");
        }
        Student studentInfo = (Student) session.getAttribute("StudentInfo");
        int stuId=studentInfo.getSid();

        //获取当前系统时间 并把他格式化 这就是结果的上传时间
        long now=System.currentTimeMillis();
        Date d=new Date();
        d.setTime(now);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int a=resultService.insert(stuId,v1,format.format(d));

        r.setCode(200);
        r.setMsg("证明材料上传成功");
//        if (a==0){
//            r.setCode(500);
//            r.setMsg("证明材料上传失败");
//        }else {
//            r.setCode(200);
//            r.setMsg("证明材料上传成功");
//        }

        return r;
    }
}

package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.College;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.ICollegeService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/college")
public class CollegeController {
    @Autowired
    private ICollegeService collegeService;

    @GetMapping("/find")
    public ResultVO find() {
        List<College> list = collegeService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }
}

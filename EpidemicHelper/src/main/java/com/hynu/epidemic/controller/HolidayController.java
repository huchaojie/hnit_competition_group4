package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Holiday;
import com.hynu.epidemic.entity.Student;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IHolidayService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/holiday")
public class HolidayController {

    @Autowired
    private IHolidayService holidayService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Holiday> list = holidayService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }

    /**
     * 提交请假申请按钮实现添加操作
     * @param v1
     * @param v2
     * @param v3
     * @return
     */
    @PostMapping ("/commit")
    /**
     * v1是请假开始时间 v2是请假结束时间 v3是请假理由
     */
    public ResultVO insert(@RequestParam("hbegintime") String v1, @RequestParam("hendtime") String v2,
                            @RequestParam("hreason") String v3, HttpSession session){
        ResultVO r =new ResultVO();
        if(null == session){
            r.setCode(500);
            r.setMsg("数据异常");
        }
        Student studentInfo = (Student) session.getAttribute("StudentInfo");
        int stuId=studentInfo.getSid();

        //获取当前系统时间  即获取htime 并把他格式化
        long now=System.currentTimeMillis();
        Date d=new Date();
        d.setTime(now);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

       int a = holidayService.insert(stuId,format.format(d),v1,v2,v3);
       if (a==0){
           r.setCode(500);
           r.setMsg("请假申请提交失败");
       }else {
           r.setCode(200);
           r.setMsg("请假申请提交成功");
       }
        return r;
    }

}

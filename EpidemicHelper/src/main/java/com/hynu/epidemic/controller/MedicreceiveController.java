package com.hynu.epidemic.controller;

import com.hynu.epidemic.entity.Medicreceive;
import com.hynu.epidemic.entity.Student;
import com.hynu.epidemic.enums.ResultEnum;
import com.hynu.epidemic.service.IMedicreceiveService;
import com.hynu.epidemic.entity.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/medicreceive")
public class MedicreceiveController {
    @Autowired
    private IMedicreceiveService medicrecriveService;

    @GetMapping("/find")
    public ResultVO find() {
        List<Medicreceive> list = medicrecriveService.find();
        if (list == null || list.isEmpty()) {
            return new ResultVO(ResultEnum.DATA_NULL);
        }
        return new ResultVO(ResultEnum.SUCCESS, list);
    }

    /**
     * 添加药物领取数据
     * @param v1   药品类型
     * @param v2   数量
     * @param session
     * @return
     */
    @PostMapping("/med")
    public ResultVO m_insert(@RequestParam("mtype") String v1, @RequestParam("mcount") int v2,
                             HttpSession session){
        ResultVO r =new ResultVO();
        if(null == session){
            r.setCode(500);
            r.setMsg("数据异常");
        }
        Student studentInfo = (Student) session.getAttribute("StudentInfo");
        int stuId=studentInfo.getSid();

        //获取当前系统时间  即获取htime 并把他格式化
        long now=System.currentTimeMillis();
        Date d=new Date();
        d.setTime(now);
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int a=medicrecriveService.m_insert(stuId,v1,v2,format.format(d));
        if (a==0){
            r.setCode(500);
            r.setMsg("药品申请提交失败");
        }else {
            r.setCode(200);
            r.setMsg("药品申请提交成功");
        }
        return r;
    }


}

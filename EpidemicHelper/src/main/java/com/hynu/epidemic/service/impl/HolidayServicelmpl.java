package com.hynu.epidemic.service.impl;


import com.hynu.epidemic.entity.Holiday;
import com.hynu.epidemic.mapper.HolidayMapper;
import com.hynu.epidemic.service.IHolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HolidayServicelmpl implements IHolidayService {
    @Autowired//自动注入
    private HolidayMapper holidayMapper;

    @Override
    public List<Holiday> find() {
        return holidayMapper.find();
    }

    @Override
    public int insert(int sid,String htime,String hbegintime, String hendtime, String hreason) {
        return holidayMapper.insert(sid,htime,hbegintime,hendtime,hreason);
    }

}

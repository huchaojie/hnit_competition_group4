package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Result;
import com.mysql.jdbc.Blob;

import java.util.List;

public interface IResultService {
    public List<Result> find();
    public int insert (int sid, Blob rprove,String datetime);
}

package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Majio;
import com.hynu.epidemic.mapper.MajioMapper;
import com.hynu.epidemic.service.IMajioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MajioServicelmpl implements IMajioService {
    @Autowired//自动注入
    private MajioMapper majioMapper;

    @Override
    public List<Majio> find(){
        return majioMapper.find();
    };
}

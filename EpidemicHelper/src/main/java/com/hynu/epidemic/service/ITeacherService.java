package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Teacher;

import java.util.List;

public interface ITeacherService {
    public List<Teacher> find();
}

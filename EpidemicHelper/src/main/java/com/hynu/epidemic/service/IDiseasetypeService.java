package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Diseasetype;

import java.util.List;

public interface IDiseasetypeService {
    public List<Diseasetype> find();
}

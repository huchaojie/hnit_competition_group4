package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Student;
import com.hynu.epidemic.mapper.StudentMapper;
import com.hynu.epidemic.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServicelmpl implements IStudentService {
    @Autowired//自动注入
    private StudentMapper studentMapper;

    @Override
    public List<Student> find(){
        return studentMapper.find();
    };

    @Override
    public  List<Student> login(String v1,String v2){
        return studentMapper.login(v1,v2);
    }
}

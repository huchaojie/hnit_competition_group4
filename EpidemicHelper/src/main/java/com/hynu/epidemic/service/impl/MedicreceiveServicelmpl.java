package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Medicreceive;
import com.hynu.epidemic.mapper.MedicreceiveMapper;
import com.hynu.epidemic.service.IMedicreceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicreceiveServicelmpl implements IMedicreceiveService {
    @Autowired//自动注入
    private MedicreceiveMapper medicreceiveMapper;

    @Override
    public List<Medicreceive> find(){
        return medicreceiveMapper.find();
    };

    @Override
    public int m_insert(int sid,String v1,int v2,String mtime){
        return medicreceiveMapper.m_insert(sid,v1,v2,mtime);
    };

}

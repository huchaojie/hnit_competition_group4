package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Class;

import java.util.List;

public interface IClassService {
    public List<Class> find();
}

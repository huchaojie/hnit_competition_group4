package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Student;

import java.util.List;

public interface IStudentService {
    public List<Student> find();
    public List<Student> login(String v1,String v2);
}

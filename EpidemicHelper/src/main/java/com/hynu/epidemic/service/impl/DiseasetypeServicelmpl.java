package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Diseasetype;
import com.hynu.epidemic.mapper.DiseasetypeMapper;
import com.hynu.epidemic.service.IDiseasetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiseasetypeServicelmpl implements IDiseasetypeService {
    @Autowired//自动注入
    private DiseasetypeMapper diseasetypeMapper;

    @Override
    public List<Diseasetype> find() {return diseasetypeMapper.find();}
}

package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Exchange;

import java.util.List;

public interface IExchangeService {
    public List<Exchange> find();
    int e_insert(String v1,String v2,String v3);
    List<Exchange> e_select(String v1,String v2);
}

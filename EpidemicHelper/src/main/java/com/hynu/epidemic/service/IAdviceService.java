package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Advice;

import java.util.List;

public interface IAdviceService {
    public List<Advice> find();
}

package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Exchange;
import com.hynu.epidemic.mapper.ExchangeMapper;
import com.hynu.epidemic.service.IExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExchangeServicelmpl implements IExchangeService {
    @Autowired//自动注入
    private ExchangeMapper exchangeMapper;

    @Override
    public List<Exchange> find(){
        return exchangeMapper.find();
    };

    @Override
    public int e_insert(String v1,String v2,String v3){return exchangeMapper.e_insert(v1,v2,v3);}

    @Override
    public List<Exchange> e_select(String v1,String v2){return exchangeMapper.e_select(v1,v2);}
}

package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Class;
import com.hynu.epidemic.mapper.ClassMapper;
import com.hynu.epidemic.service.IClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassServicelmpl implements IClassService {
    @Autowired//自动注入
    private ClassMapper classMapper;

    @Override
    public List<Class> find(){
        return classMapper.find();
    };
}

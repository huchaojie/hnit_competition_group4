package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Teacher;
import com.hynu.epidemic.mapper.TeacherMapper;
import com.hynu.epidemic.service.ITeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServicelmpl implements ITeacherService {
    @Autowired//自动注入
    private TeacherMapper teacherMapper;

    @Override
    public List<Teacher> find(){
        return teacherMapper.find();
    };
}

package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.Majio;

import java.util.List;

public interface IMajioService {
    public List<Majio> find();
}

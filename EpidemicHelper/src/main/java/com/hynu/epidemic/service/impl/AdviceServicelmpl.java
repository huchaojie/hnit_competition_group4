package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Advice;
import com.hynu.epidemic.mapper.AdviceMapper;
import com.hynu.epidemic.service.IAdviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdviceServicelmpl implements IAdviceService {
    @Autowired//自动注入
    private AdviceMapper adviceMapper;

    @Override
    public List<Advice> find(){
        return adviceMapper.find();
    };
}

package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.College;
import com.hynu.epidemic.mapper.CollegeMapper;
import com.hynu.epidemic.service.ICollegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollegeServicelmpl implements ICollegeService {
    @Autowired//自动注入
    private CollegeMapper collegeMapper;

    @Override
    public List<College> find() {
        return collegeMapper.find();
    }

}

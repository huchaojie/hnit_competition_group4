package com.hynu.epidemic.service.impl;

import com.hynu.epidemic.entity.Result;
import com.hynu.epidemic.mapper.ResultMapper;
import com.hynu.epidemic.service.IResultService;
import com.mysql.jdbc.Blob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServicelmpl implements IResultService {
    @Autowired//自动注入
    private ResultMapper resultMapper;

    @Override
    public List<Result> find(){
        return resultMapper.find();
    };

    @Override
    public int insert(int sid, Blob rprove,String datetime){
        return resultMapper.insert(sid,rprove,datetime);
    }
}

package com.hynu.epidemic.service;

import com.hynu.epidemic.entity.College;

import java.util.List;

public interface ICollegeService {
    public List<College> find();
}

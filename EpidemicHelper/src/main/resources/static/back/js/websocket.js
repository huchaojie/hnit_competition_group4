function openWebSocket(mno) {

    var socket;
    if (typeof (WebSocket) == undefined) {
        alert("对不起，您的浏览器不支持WebSocket通信....");
        return;
    }

    socket = new WebSocket("ws://localhost:8020/websocket/" + mno);
    socket.onopen = function () {
        console.log("连接成功.....")
    }
    socket.onclose = function () {
        console.log("socket关闭.....")
    }
    socket.onerror = function () {
        console.log("服务器连接出错。。。。");
    }
    socket.onmessage = function (msg) {
        if (msg.data == "101") {
            //说明已经在其他的地方登陆
            alert("对不起，您的账号已经在其他地方登陆，若非本人操作，请及时修改密码...");
            window.location.href = "../index.html";
        } else {
            console.log(msg);
        }
    }
}